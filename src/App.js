import './App.css';
import bootstrap from 'bootstrap/dist/css/bootstrap.css'
import React from "react";
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
} from "react-router-dom";
import PostsPage from "./Pages/PostsPage";


function App() {
    return (
        <div className="App">
            <Router>
                <div>
                    <div>
                        <Link to="/">Home</Link>
                    </div>
                    <div>
                        <Link to="/posts">Posts</Link>
                    </div>
                    <hr/>
                </div>

                <Switch>
                    <Route path="/posts">
                        <PostsPage/>
                    </Route>
                    <Route path="/home">
                        <h3>Home</h3>
                    </Route>
                </Switch>
            </Router>
        </div>
    );
}

export default App;
