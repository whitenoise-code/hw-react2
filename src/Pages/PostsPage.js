﻿import React, {useState, useEffect} from 'react';
import axios from 'axios'

export default function PostsPage() {

    const [posts, setPosts] = useState([])

    useEffect(() => {

        axios.get(`${process.env.REACT_APP_API_URL}/posts`)
            .then(response => {
                const posts = response.data
                console.log(posts)
                setPosts(posts)
            })
    });

    return (
        <div>{posts.map(p=> <li key={p.id}>{p.title}</li>)}</div>
    )
}